from http.server import BaseHTTPRequestHandler
from http.server import HTTPServer
from urllib.parse import urlparse, parse_qs
import logging
import random
import string

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


class HttpGet(BaseHTTPRequestHandler):

    def do_GET(self):
        msg = urlparse(self.path)
#        self.server_version = "Server: Apache 2.2.0 OpenBSD 0.9"
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write('<html><head><meta charset="utf-8">'.encode())
        self.wfile.write('<title>HTTP GET Query</title></head>'.encode())
        self.wfile.write('<body><h1>GET query info</h1>'.encode())
        self.wfile.write('<br>'.encode())
        self.wfile.write(str(msg.query).encode())
        self.wfile.write('<br>'.encode())
#        self.wfile.write(str(id_generator(7)).encode())
        self.wfile.write(parse_qs(msg.query)['z'][0].encode())
        self.wfile.write('<br>'.encode())
        self.wfile.write('</body>'.encode())
        logging.debug('Another query %s', msg.query)

if __name__ == '__main__':
    logging.basicConfig(filename='sqlmap.log', encoding='utf-8', level=logging.DEBUG)
    server = HTTPServer(('localhost', 1080), HttpGet)
    print('HTTP Server at http://localhost:1080')
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        server.server_close()

